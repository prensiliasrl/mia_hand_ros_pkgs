/*
@licstart  The following is the entire license notice for the
JavaScript code in this file.

Copyright (C) 1997-2019 by Dimitri van Heesch

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

@licend  The above is the entire license notice
for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "mia_hand_bringup", "index.html", [
    [ "mia_hand_bringup", "md_home_prensila_ros_ws_src_mia_hand_ros_pkgs_mia_hand_bringup__r_e_a_d_m_e.html", null ],
    [ "File Members", "globals.html", [
      [ "All", "globals.html", null ],
      [ "Functions", "globals_func.html", null ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"globals.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';