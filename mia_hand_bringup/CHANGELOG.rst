^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Changelog for package mia_hand_bringup
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

1.0.2 (2022-02-07)
------------------

1.0.1 (2022-02-07)
------------------
* Merge branch 'master' of bitbucket.org:prensiliasrl/mia_hand_ros_pkgs
* Update trajectory velocity controllers (also for moveit).
* Contributors: Andrea Burani, frcini

1.0.0 (2022-01-25)
------------------
* 1.0.0
* Updated CHANGELOG files.
* Updated CHANGELOG files.
* Added srv dependency to mia_hand_driver and joint_state_interface to mmia_hand_description. Added wiki url links.
* 1.0.0
* Added ChangeLog files of each pkg.
* Initial commit.
* Fix package xml files.
* Initial commit.
* Contributors: Andrea Burani, frcini
