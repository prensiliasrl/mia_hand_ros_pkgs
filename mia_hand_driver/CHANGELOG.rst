^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Changelog for package mia_hand_driver
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

1.0.2 (2022-02-07)
------------------
* Added Threads package finding in CMake.
* Contributors: Andrea Burani

1.0.1 (2022-02-07)
------------------
* Forced CMake to find at least version 1.0.0 of libserial.
* Contributors: Andrea Burani

1.0.0 (2022-01-25)
------------------
* Changed #include directive for libserial.
* 1.0.0
* Updated CHANGELOG files.
* Limited max motor PWM in speed command.
* Created libserial IMPORTED_TARGET.
* Updated CHANGELOG files.
* Added srv dependency to mia_hand_driver and joint_state_interface to mmia_hand_description. Added wiki url links.
* Added license text at the beginning of each file.
* 1.0.0
* Added ChangeLog files of each pkg.
* Initial commit.
* Fix package xml files.
* Initial commit.
* Contributors: Andrea Burani, frcini
