var hierarchy =
[
    [ "mia_hand::CppDriver", "classmia__hand_1_1_cpp_driver.html", null ],
    [ "mia_hand::FingerSerialInfo", "structmia__hand_1_1_finger_serial_info.html", null ],
    [ "mia_hand::ROSDriver", "classmia__hand_1_1_r_o_s_driver.html", null ],
    [ "SerialPort", null, [
      [ "mia_hand::SerialPort", "classmia__hand_1_1_serial_port.html", null ]
    ] ]
];