# mia_hand_driver
### ROS driver for Prensilia s.r.l Mia Hand device
To be able to use this package, **LibSerial** library needs to be installed on your device.

This can be done by running `sudo apt install libserial-dev`.

If you prefer to install LibSerial from source, please refer to [LibSerial documentation](https://libserial.readthedocs.io/en/latest/install.html).

This package also depends on **mia_hand_msgs** package, so both of them should be cloned in the same catkin workspace.
