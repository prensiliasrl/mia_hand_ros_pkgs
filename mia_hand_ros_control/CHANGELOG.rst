^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Changelog for package mia_hand_ros_control
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

1.0.2 (2022-02-07)
------------------

1.0.1 (2022-02-07)
------------------
* Merge branch 'master' of bitbucket.org:prensiliasrl/mia_hand_ros_pkgs
* Update trajectory velocity controllers (also for moveit).
* Contributors: Andrea Burani, frcini

1.0.0 (2022-01-25)
------------------
* 1.0.0
* Updated CHANGELOG files.
* Updated CHANGELOG files.
* Added srv dependency to mia_hand_driver and joint_state_interface to mmia_hand_description. Added wiki url links.
* Added license file in the repo and license text as header of each file.
* Added licence file and licence text as header of each file.
* 1.0.0
* Added ChangeLog files of each pkg.
* Initial commit.
* Fix package xml files.
* Initial commit.
* Contributors: Andrea Burani, frcini
