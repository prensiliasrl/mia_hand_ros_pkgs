var indexSectionsWithContent =
{
  0: "acdefghijlmnoprstuvwx~",
  1: "jm",
  2: "mt",
  3: "cmr",
  4: "acdfghijmprstuw~",
  5: "acdefghijlmnoprstuvwx",
  6: "c",
  7: "epv",
  8: "m"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "enums",
  7: "enumvalues",
  8: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Enumerations",
  7: "Enumerator",
  8: "Pages"
};

