var hierarchy =
[
    [ "mia_hand::Joint_index_mapping", "structmia__hand_1_1_joint__index__mapping.html", null ],
    [ "transmission_interface::MiaTransmissionHandle", "classtransmission__interface_1_1_mia_transmission_handle.html", [
      [ "transmission_interface::MiaActuatorToJointPositionHandle", "classtransmission__interface_1_1_mia_actuator_to_joint_position_handle.html", null ],
      [ "transmission_interface::MiaActuatorToJointStateHandle", "classtransmission__interface_1_1_mia_actuator_to_joint_state_handle.html", null ],
      [ "transmission_interface::MiaActuatorToJointVelocityHandle", "classtransmission__interface_1_1_mia_actuator_to_joint_velocity_handle.html", null ],
      [ "transmission_interface::MiaJointToActuatorPositionHandle", "classtransmission__interface_1_1_mia_joint_to_actuator_position_handle.html", null ],
      [ "transmission_interface::MiaJointToActuatorStateHandle", "classtransmission__interface_1_1_mia_joint_to_actuator_state_handle.html", null ],
      [ "transmission_interface::MiaJointToActuatorVelocityHandle", "classtransmission__interface_1_1_mia_joint_to_actuator_velocity_handle.html", null ]
    ] ],
    [ "ResourceManager", null, [
      [ "transmission_interface::MiaTransmissionInterface< MiaActuatorToJointPositionHandle >", "classtransmission__interface_1_1_mia_transmission_interface.html", [
        [ "transmission_interface::MiaActuatorToJointPositionInterface", "classtransmission__interface_1_1_mia_actuator_to_joint_position_interface.html", null ]
      ] ],
      [ "transmission_interface::MiaTransmissionInterface< MiaActuatorToJointStateHandle >", "classtransmission__interface_1_1_mia_transmission_interface.html", [
        [ "transmission_interface::MiaActuatorToJointStateInterface", "classtransmission__interface_1_1_mia_actuator_to_joint_state_interface.html", null ]
      ] ],
      [ "transmission_interface::MiaTransmissionInterface< MiaActuatorToJointVelocityHandle >", "classtransmission__interface_1_1_mia_transmission_interface.html", [
        [ "transmission_interface::MiaActuatorToJointVelocityInterface", "classtransmission__interface_1_1_mia_actuator_to_joint_velocity_interface.html", null ]
      ] ],
      [ "transmission_interface::MiaTransmissionInterface< MiaJointToActuatorPositionHandle >", "classtransmission__interface_1_1_mia_transmission_interface.html", [
        [ "transmission_interface::MiaJointToActuatorPositionInterface", "classtransmission__interface_1_1_mia_joint_to_actuator_position_interface.html", null ]
      ] ],
      [ "transmission_interface::MiaTransmissionInterface< MiaJointToActuatorStateHandle >", "classtransmission__interface_1_1_mia_transmission_interface.html", [
        [ "transmission_interface::MiaJointToActuatorStateInterface", "classtransmission__interface_1_1_mia_joint_to_actuator_state_interface.html", null ]
      ] ],
      [ "transmission_interface::MiaTransmissionInterface< MiaJointToActuatorVelocityHandle >", "classtransmission__interface_1_1_mia_transmission_interface.html", [
        [ "transmission_interface::MiaJointToActuatorVelocityInterface", "classtransmission__interface_1_1_mia_joint_to_actuator_velocity_interface.html", null ]
      ] ],
      [ "transmission_interface::MiaTransmissionInterface< HandleType >", "classtransmission__interface_1_1_mia_transmission_interface.html", null ]
    ] ],
    [ "RobotHW", null, [
      [ "mia_hand::MiaHWInterface", "classmia__hand_1_1_mia_h_w_interface.html", null ]
    ] ],
    [ "Transmission", null, [
      [ "transmission_interface::MiaIndexTransmission", "classtransmission__interface_1_1_mia_index_transmission.html", null ],
      [ "transmission_interface::MiaMrlTransmission", "classtransmission__interface_1_1_mia_mrl_transmission.html", null ],
      [ "transmission_interface::MiaThfleTransmission", "classtransmission__interface_1_1_mia_thfle_transmission.html", null ]
    ] ]
];