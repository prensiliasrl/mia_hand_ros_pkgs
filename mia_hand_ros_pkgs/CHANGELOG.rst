^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Changelog for package mia_hand_ros_pkgs
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

1.0.2 (2022-02-07)
------------------

1.0.1 (2022-02-07)
------------------

1.0.0 (2022-01-25)
------------------
* 1.0.0
* Updated CHANGELOG files.
* Updated CHANGELOG files.
* Added srv dependency to mia_hand_driver and joint_state_interface to mmia_hand_description. Added wiki url links.
* 1.0.0
* Adjusted Cmake file of mia_hand_ros_pkgs metapkg for release.
* Added ChangeLog files of each pkg.
* Initial commit.
* Fix package xml files.
* Initial commit.
* Contributors: Andrea Burani, frcini
