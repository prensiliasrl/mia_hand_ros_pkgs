^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Changelog for package mia_hand_moveit_config
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

1.0.2 (2022-02-07)
------------------

1.0.1 (2022-02-07)
------------------

1.0.0 (2022-01-25)
------------------
* 1.0.0
* Updated CHANGELOG files.
* Updated CHANGELOG files.
* 1.0.0
* Added ChangeLog files of each pkg.
* Initial commit.
* Fix package xml files.
* Initial commit.
* Contributors: Andrea Burani, frcini
