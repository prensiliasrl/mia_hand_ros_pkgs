var classmia__hand_1_1thumb__opp__passive__joint =
[
    [ "thumb_opp_passive_joint", "classmia__hand_1_1thumb__opp__passive__joint.html#a48e5204011c8c4761a6f948e224de00a", null ],
    [ "~thumb_opp_passive_joint", "classmia__hand_1_1thumb__opp__passive__joint.html#ad59cc131507eeaab9d7bccf5ce231494", null ],
    [ "GetThumbOppPosition", "classmia__hand_1_1thumb__opp__passive__joint.html#a0ec8baa18e5b0b6418847c305a997c24", null ],
    [ "getURDF", "classmia__hand_1_1thumb__opp__passive__joint.html#a5af9459730bd07dda713a115fe9b6871", null ],
    [ "init", "classmia__hand_1_1thumb__opp__passive__joint.html#a629774a0664e6d0c3182823cc3169601", null ],
    [ "LoadURDFInfo", "classmia__hand_1_1thumb__opp__passive__joint.html#a2fb31cb4e9e226364c934122668fe986", null ],
    [ "updateThOppJointLimits", "classmia__hand_1_1thumb__opp__passive__joint.html#a07b07c0ee6bf83f94aaf232c4e97db01", null ],
    [ "j_index_name", "classmia__hand_1_1thumb__opp__passive__joint.html#a5368d6a0fdf658476b4d6d476284fa80", null ],
    [ "j_thumb_name", "classmia__hand_1_1thumb__opp__passive__joint.html#a8fb10e0327b4f325f79694c1051902e3", null ],
    [ "n", "classmia__hand_1_1thumb__opp__passive__joint.html#a743945b791b9498c35d3ba614aa7b047", null ],
    [ "robot_description_", "classmia__hand_1_1thumb__opp__passive__joint.html#a7cd1c447a3579712e421ff6802b3e720", null ],
    [ "robot_description_full", "classmia__hand_1_1thumb__opp__passive__joint.html#a44229865e53e995d5012dd0ac7275a64", null ],
    [ "robot_namespace_", "classmia__hand_1_1thumb__opp__passive__joint.html#af6592e6da1f7b1a54dac5a2a3dd149b0", null ],
    [ "th_max_pos_", "classmia__hand_1_1thumb__opp__passive__joint.html#afb38fd7658d4927ce46a7df0418d1ce2", null ],
    [ "th_min_pos_", "classmia__hand_1_1thumb__opp__passive__joint.html#a610a0a1884f25f4cc52db342a83e03a9", null ],
    [ "th_opp_offset_", "classmia__hand_1_1thumb__opp__passive__joint.html#ae76ebb83f068d5a7f5aab34ee33ffabc", null ],
    [ "th_opp_scale_", "classmia__hand_1_1thumb__opp__passive__joint.html#a86f5a1be8d31b93f6f24407806b5b12e", null ],
    [ "th_opp_start_close_index_angle_", "classmia__hand_1_1thumb__opp__passive__joint.html#a0d540e3b9990fb1e793ef27d11f1fe3a", null ],
    [ "th_opp_stop_close_index_angle_", "classmia__hand_1_1thumb__opp__passive__joint.html#a096d4369cf7c202356b3c51643abe93c", null ]
];